import math
import numpy as np

class User:
    def __init__(self, userid, hour1, hour2, hour3, lat, lon, posts):
        self.userid = userid
        self.hour1 = hour1
        self.hour2 = hour2
        self.hour3 = hour3
        self.lat = lat
        self.lon = lon
        self.posts = posts
        self.friends = []

        # lets ignore training docs where there is no location data
        if self.lat == 0 and self.lon == 0:
            self.lat = None
            self.lon = None

        # geodatic location system
        if self.lat == None or self.lon == None:
            self.x = None
            self.y = None
            self.z = None
        else:
            lat_rad = self.lat * math.pi / 180
            lon_rad = self.lon * math.pi / 180
            self.x = math.cos(lat_rad)*math.cos(lon_rad)
            self.y = math.cos(lat_rad)*math.sin(lon_rad)
            self.z = math.sin(lat_rad)

        # calculated features
        self.avg_friend_loc = None
        self.mode_friend_loc = None
        self.mode_friend_loc_weighted = None
        self.friend_count = None
        self.median_friend_loc = None

    def get_userid(self):
        return self.userid

    def get_hour1(self):
        return self.hour1

    def get_hour2(self):
        return self.hour2

    def get_hour3(self):
        return self.hour3

    def get_lat(self):
        return self.lat

    def get_lon(self):
        return self.lon

    def get_posts(self):
        return self.posts

    def get_friends(self):
        return self.friends

    def get_location(self, xyz_loc=False):
        if xyz_loc:
            return [self.x, self.y, self.z]
        else:
            return [self.lat, self.lon]

    def get_x_coord(self):
        return self.x

    def get_y_coord(self):
        return self.y

    def get_z_coord(self):
        return self.z

    def get_friends_to_depth(self, train_coll, depth, curr_depth):
        friends = []
        if curr_depth > depth:
            return friends
        for userid in self.friends:
            try:
                friend = train_coll[userid]
                friends += [userid] + friend.get_friends_to_depth(train_coll, depth, curr_depth+1)
            except KeyError:
                pass
        return friends

    def reject_outliers(self, friendids, train_coll,m=1):
        friend_locs = {}
        for userid in friendids:
            try:
                user = train_coll[userid]
                if user.get_x_coord() != None:
                    friend_locs[userid] = [user.get_x_coord(), user.get_y_coord(), user.get_z_coord()]
            except KeyError:
                pass

        if friend_locs.keys() == []:
            return []

        friend_locs_np = np.array(friend_locs.values())
        x_mean = np.mean(friend_locs_np[:,0])
        x_std = np.std(friend_locs_np[:,0])
        y_mean = np.mean(friend_locs_np[:,1])
        y_std = np.std(friend_locs_np[:,1])
        z_mean = np.mean(friend_locs_np[:,2])
        z_std = np.std(friend_locs_np[:,2])

        friend_locs_fixed = []
        for (userid, friend_loc) in friend_locs.iteritems():
            if isKeeper(friend_loc[0], x_mean, x_std, m) and isKeeper(friend_loc[1], y_mean, y_std, m) and isKeeper(friend_loc[2], z_mean, z_std, m):
                friend_locs_fixed += [userid]
        return friend_locs_fixed

    def calc_features(self, train_coll):
        friends_to_depth = self.get_friends_to_depth(train_coll, 1, 1)
        if len(self.friends) >= 2:
            if len(friends_to_depth) > 10:
                friends_to_depth = self.reject_outliers(friends_to_depth, train_coll)
        else:
            friends_to_depth = self.get_friends_to_depth(train_coll, 2, 1)
            if len(friends_to_depth) > 30:
                friends_to_depth = self.reject_outliers(friends_to_depth, train_coll,1)

        self.avg_friend_loc = self.calc_avg_friend_loc(train_coll, friends_to_depth)
        self.mode_friend_loc = self.calc_mode_friend_loc_weighted(train_coll, 1, friends_to_depth)
        self.mode_friend_loc_weighted = self.calc_mode_friend_loc_weighted(train_coll, 1, friends_to_depth) + self.calc_mode_friend_loc_weighted(train_coll, 2, friends_to_depth) + self.calc_mode_friend_loc_weighted(train_coll, 3, friends_to_depth)
        self.friend_count = self.calc_friend_count()
        self.median_friend_loc = self.calc_median_friend_loc(train_coll, friends_to_depth)

    def get_features(self, inc_avg_friend_loc=False, inc_mode_friend_loc=False,
        inc_mode_friend_loc_weighted=False, inc_friend_count=False, 
        inc_median_friend_loc=False):
        features = self.get_basic_features()
        if inc_avg_friend_loc:
            features += self.avg_friend_loc
        if inc_mode_friend_loc:
            features += self.mode_friend_loc
        if inc_mode_friend_loc_weighted:
            features += self.mode_friend_loc_weighted
        if inc_friend_count:
            features += self.friend_count
        if inc_median_friend_loc:
            features += self.median_friend_loc

        return features

    def get_basic_features(self):
        return [self.hour1, self.hour2, self.hour3, self.posts]

    def calc_avg_friend_loc(self, train_coll, friends):
        x_total = 0
        y_total = 0
        z_total = 0
        friends_with_loc = 0

        for userid in friends:
            try:
                friend = train_coll[userid]
                xyz_loc = friend.get_location(True)
                x_total += xyz_loc[0]
                y_total += xyz_loc[1]
                z_total += xyz_loc[2]
                friends_with_loc += 1
            except (KeyError, TypeError) as e:
                pass
        if friends_with_loc != 0:
            return xyz_to_latlon([x_total / friends_with_loc, y_total / friends_with_loc,
                z_total / friends_with_loc])
        else:
            return [0, 0]
    
    def calc_friend_count(self):
        return [len(self.friends)]

    def calc_mode_friend_loc_weighted(self, train_coll, hour, friends):
        loc_freqs = {}
        for userid in friends:
            try:
                friend = train_coll[userid]
                loc = (round(friend.get_x_coord(),2), round(friend.get_y_coord(),2), round(friend.get_z_coord(),2))
                # loc = (round(friend.get_lat(),-1), round(friend.get_lon(),-1))

                if (hour==1):
                    time_sim_score = 13-time_diff(self.hour1, friend.get_hour1()) 
                elif(hour==2):
                    time_sim_score = 13-time_diff(self.hour2, friend.get_hour2()) 
                else:
                    time_sim_score = 13-time_diff(self.hour3, friend.get_hour3()) 
                try:
                    loc_freqs[loc] += time_sim_score
                except KeyError:
                    loc_freqs[loc] = time_sim_score
            except (KeyError, TypeError) as e:
                pass
        if (len(loc_freqs)>0):
            mode = sorted(loc_freqs, key=loc_freqs.get, reverse=True)[0]
            
            return xyz_to_latlon(mode)
            # return [mode[0], mode[1]]
        else:
            return [0,0]

    def calc_median_friend_loc(self, train_coll, friends):
        xs = []
        ys = []
        zs = []

        for userid in friends:
            try:
                friend = train_coll[userid]
                if friend.get_x_coord() != None:
                    xs += [friend.get_x_coord()]
                    ys += [friend.get_y_coord()]
                    zs += [friend.get_z_coord()]
            except KeyError:
                pass

        if len(xs) > 0:
            return xyz_to_latlon([xs[len(xs) / 2], ys[len(ys) / 2], zs[len(zs) / 2]])
        else:
            return [0,0]


    def calc_friends_of_friends(self, train_coll):
        n  = 0
        for userid in self.friends:
            try:
                friend = train_coll[userid]
                n+= friend.calc_friend_count(train_coll)[0]
            except KeyError:
                pass
        return [n]

    def set_lat(self, lat):
        self.lat = lat

    def set_lon(self, lon):
        self.lon = lon

    def add_friend(self, classid):
        self.friends.append(classid)

    def has_friend(self, classid):
        return classid in self.friends

class UserColl:
    def __init__(self):
        self.users = {}

    def __getitem__(self, key):
        return self.users[key]

    def add_user(self, user):
        self.users[user.get_userid()] = user

    def get_users(self):
        return self.users

    def __len__(self):
        return len(self.users)

    def __iter__(self):
        return UserCollInorderIterator(self)

class UserCollInorderIterator:
    def __init__(self, user_coll):
        self.user_coll = user_coll
        self.keys = sorted(user_coll.get_users().keys())
        self.i = 0

    def __iter__(self):
        return self

    def next(self):
        if self.i >= len(self.keys):
            raise StopIteration
        user = self.user_coll[self.keys[self.i]]
        self.i += 1
        return user

def parse_user_data(user_fname, user_coll):
    with open(user_fname) as f:
        next(f)
        for line in f:
            data = [(i.strip()) for i in line.split(',')]
            if len(data) == 7:
                user_coll.add_user(User(int(data[0]), int(data[1]), int(data[2]),
                    int(data[3]), float(data[4]), float(data[5]), int(data[6])))
            elif len(data) == 5:
                user_coll.add_user(User(int(data[0]), int(data[1]), int(data[2]),
                    int(data[3]), None, None, int(data[4])))

def parse_graph_data(graph_fname, user_coll):
    with open(graph_fname) as f:
        for line in f:
            ids = [int(i) for i in line.split()]
            try:
                user_coll[ids[0]].add_friend(ids[1])
            except KeyError:
                pass

def parse_train_data(user_fname, graph_fname):
    user_coll = UserColl()
    parse_user_data(user_fname, user_coll)
    parse_graph_data(graph_fname, user_coll)
    for user in user_coll:
        user.calc_features(user_coll)
    return user_coll

def parse_test_data(user_fname, graph_fname, train_coll):
    user_coll = UserColl()
    parse_user_data(user_fname, user_coll)
    parse_graph_data(graph_fname, user_coll)
    for user in user_coll:
        user.calc_features(train_coll)
    return user_coll

def isKeeper(val, mean, std, m=1):
    if abs(val - mean) < m * std:
        return True
    return False

def xyz_to_latlon(xyz):
    x = xyz[0]
    y = xyz[1]
    z = xyz[2]

    lon_rad = math.atan2(y,x)
    hyp = math.sqrt(x*x+y*y)
    lat_rad = math.atan2(z,hyp)

    return [lat_rad * 180 / math.pi, lon_rad * 180 / math.pi]

def latlon_to_xyz(latlon):
    lat_rad = latlon[0] * math.pi / 180
    lon_rad = latlon[1] * math.pi / 180
    return [math.cos(lat_rad)*math.cos(lon_rad), math.cos(lat_rad)*math.sin(lon_rad), math.sin(lat_rad)]

def time_diff(t1, t2):
    return min(abs(t1-t2), abs((t1+24)-t2), abs(t1-(t2+24)))

if __name__ == '__main__':
    import argparse

    # parse args
    parser = argparse.ArgumentParser(description="Parse user and graph data")
    parser.add_argument("user_fname", help="Filename of txt file with user data.")
    parser.add_argument("graph_fname", help="Filename of txt file with relationship data")
    args = parser.parse_args()

    user_coll = parse_input_data(args.user_fname, args.graph_fname)

    for user in user_coll:
        print "id:%d, hour1:%d, hour2:%d, hour3:%d, lat:%d, lon:%d, posts:%d" % (
            user.get_userid(), user.get_hour1(), user.get_hour2(), 
                user.get_hour3(), user.get_lat(), user.get_lon(), user.get_posts())
        print "\tfriends: %s" % (user.get_friends())
