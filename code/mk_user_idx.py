from code.util import user_coll
import pickle

def index_files(train_fname, test_fname, graph_fname, train_idx_fname, test_idx_fname):
    user_train_coll = user_coll.parse_train_data(train_fname, graph_fname)
    pickle.dump(user_train_coll, open(train_idx_fname, "wb"))
    user_test_coll = user_coll.parse_test_data(test_fname, graph_fname, user_train_coll)
    pickle.dump(user_test_coll, open(test_idx_fname, "wb"))

if __name__ == '__main__':
    import argparse

    # parse args
    parser = argparse.ArgumentParser(description="Make the user collection index")
    parser.add_argument("train_fname", help="Filename of txt file with user training data.")
    parser.add_argument("test_fname", help="Filename of txt file with user test data.")
    parser.add_argument("graph_fname", help="Filename of txt file with relationship data")
    parser.add_argument("train_idx_fname", help="Filename of the train index file to output")
    parser.add_argument("test_idx_fname", help="Filename of the test index file to output")
    args = parser.parse_args()

    index_files(args.train_fname, args.test_fname, args.graph_fname, args.train_idx_fname, args.test_idx_fname)