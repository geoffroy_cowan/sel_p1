import pickle
from code.util import user_coll
from methods.machine_learner import MachineLearner

def load_pickle_obj(fname):
    with open(fname, "rb") as f:
        return pickle.load(f)

def output_results(user_test_coll, fname):
    with open(fname, "wb") as f:
        f.write("Id,Lat,Lon\n")
        for user in user_test_coll:
            f.write("%s,%s,%s\n" % (user.get_userid(), user.get_lat(), user.get_lon()))


def main(args):
    import argparse

    # parse args
    parser = argparse.ArgumentParser(description="Predict location of test data")
    parser.add_argument("train_idx", help="Filename of training index")
    parser.add_argument("test_idx", help="Filename of test index")
    parser.add_argument("output_fname", help="Filename of output file")
    parser.add_argument("method", choices=['example','ols','las','rid','els'], help="Method to predict locations")
    args = parser.parse_args()
    #do prediction
    predict(args.train_idx, args.test_idx, args.output_fname, args.method, True, True, True, False, False)


def predict(train_idx, test_idx, output_fname, method, inc_avg_friend_loc=False, 
    inc_mode_friend_loc=False, inc_mode_friend_loc_weighted=False, 
    inc_friend_count=False, inc_median_friend_loc=False):
    if method == "example":
        from methods.example_learner import ExampleLearner
        ml = ExampleLearner()
    elif method == "ols":
        from methods.ordinary_least_squares import OrdinaryLeastSquares
        ml = OrdinaryLeastSquares(inc_avg_friend_loc, inc_mode_friend_loc, 
            inc_mode_friend_loc_weighted, inc_friend_count, inc_median_friend_loc)
    elif method == "las":
        from methods.lasso import Lasso
        ml = Lasso(inc_avg_friend_loc, inc_mode_friend_loc, 
            inc_mode_friend_loc_weighted, inc_friend_count, inc_median_friend_loc)
    elif method == "rid":
        from methods.ridge import Ridge
        ml = Ridge(inc_avg_friend_loc, inc_mode_friend_loc, 
            inc_mode_friend_loc_weighted, inc_friend_count, inc_median_friend_loc)
    elif method == "els":
        from methods.elastic import Elastic
        ml = Elastic(inc_avg_friend_loc, inc_mode_friend_loc, 
            inc_mode_friend_loc_weighted, inc_friend_count, inc_median_friend_loc)

    if isinstance(ml, MachineLearner):
        user_train_coll = load_pickle_obj(train_idx)
        user_test_coll = load_pickle_obj(test_idx)
        ml.train(user_train_coll)
        for user in user_test_coll:
            ml.predict(user)
        output_results(user_test_coll, output_fname)



if __name__ == '__main__':
    import sys
    main(sys.argv[1:])