import os
import random
import math
from code import do_predict
from code import mk_user_idx
from code.util import user_coll

def main(method):
	actual = False

	#create folder for output
	temp_path = make_folder()

	#file names
	temp_train_fname = temp_path + "/temp_train.txt"
	temp_train_idx_fname = temp_path + "/temp_train.idx"
	temp_test_fname = temp_path + "/temp_test.txt"
	temp_test_idx_fname = temp_path + "/temp_test.idx"
	temp_results_fname = temp_path + "/results.txt"
	graph_fname = "./data/graph.txt"
	train_fname = "./data/posts-train.txt"
	test_fname = "./data/posts-test-x.txt"

	#pick 500 samples at random for test set
	random.seed(133)
	test_lines = random_set(1000, 1, 49813)

	#split training file into temporart test and train files
	if(not actual):
		answers = split_train_file(train_fname, temp_train_fname, temp_test_fname, test_lines)
	else:
		copy_train_file(train_fname, test_fname, temp_train_fname, temp_test_fname)
	#make indeces from temp files
	mk_user_idx.index_files(temp_train_fname, temp_test_fname, graph_fname, temp_train_idx_fname, temp_test_idx_fname)

	for inc_avg_friend_loc in (True, False):
		for inc_mode_friend_loc in (True, False):
			for inc_mode_friend_loc_weighted in (True, False):
				for inc_friend_count in (True, False):
					for inc_median_friend_loc in (True, False):
						do_predict.predict(temp_train_idx_fname, temp_test_idx_fname, 
							temp_results_fname, method, inc_avg_friend_loc, 
							inc_mode_friend_loc, inc_mode_friend_loc_weighted, 
							inc_friend_count, inc_median_friend_loc)

						#print error value
						if (not actual):
							print "inc_avg_friend_loc = %s\t inc_mode_friend_loc = %s\t inc_mode_friend_loc_weighted = %s\t inc_friend_count = %s\t inc_median_friend_loc = %s\n\trms_error = %f" % (
								inc_avg_friend_loc, inc_mode_friend_loc, 
								inc_mode_friend_loc_weighted, inc_friend_count, 
								inc_median_friend_loc, rms_error(answers, temp_results_fname))


#creates unique output directory
def make_folder():
	flag = True
	count = 0
	temp_path = ""
	while flag:
		temp_path = "test_results_"+"%03d"%count
		if not os.path.exists(temp_path):
			os.makedirs(temp_path)
			flag = False
		else:
			count += 1
	return temp_path

#returns random set of n size with lower/upper bounds on entries
def random_set(n, lower, upper):
	s = set()
	while (len(s)<n):
		s.add(random.randint(lower,upper))
	return s

#create new temporary test/train files
def split_train_file(train_fname, temp_train_fname, temp_test_fname, test_lines):
	test_answers = {}
	temp_train_file = open(temp_train_fname, 'w')
	temp_test_file = open(temp_test_fname, 'w')
	#write header line to test file
	temp_test_file.write("Id,Hour1,Hour2,Hour3,Posts\n")
	#output train data to temp test or train file
	c_line = 0
	with open(train_fname) as f:
		for line in f:
			if c_line in test_lines:
				#write test line file	
				data = [(i.strip()) for i in line.split(',')]
				test_answers[int(data[0])] = (float(data[4]),float(data[5]))
				temp_test_file.write(','.join(data[0:4]+data[-1:]) + '\n')
			else:
				#write train line file
				temp_train_file.write(line)
			c_line += 1
	temp_train_file.close()
	temp_test_file.close()
	return test_answers

#make duplicates of test/train data
def copy_train_file(train_fname, test_fname, temp_train_fname, temp_test_fname):
	temp_train_file = open(temp_train_fname, 'w')
	temp_test_file = open(temp_test_fname, 'w')
	with open(train_fname) as f:
		for line in f:
				temp_train_file.write(line)
	with open(test_fname) as f:
		for line in f:
				temp_test_file.write(line)
	temp_train_file.close()
	temp_test_file.close()

#calc root mean squared error over test cases
def rms_error(test_answers, temp_results_fname):
	n = 0
	diff_sq = 0
	with open(temp_results_fname) as f:
		next(f)
		for line in f:
			data = [(i.strip()) for i in line.split(',')]
			c_id =  int(data[0])
			lat = float(data[1])
			lon = float(data[2])
			expected = test_answers[c_id]
			diff_sq += (lat-expected[0])**2
			diff_sq += (lon-expected[1])**2
			n += 2
	return (math.sqrt(diff_sq/n))




if __name__ == '__main__':
	import sys
	main(sys.argv[1])
    