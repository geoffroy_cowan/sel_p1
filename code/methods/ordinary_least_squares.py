from sklearn import linear_model
from sklearn_user_machine_learner import SkLearnUserMachineLearner

class OrdinaryLeastSquares(SkLearnUserMachineLearner):

    def get_regressor(self):
        return linear_model.LinearRegression()