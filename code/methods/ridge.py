from sklearn import linear_model
from sklearn_user_machine_learner import SkLearnUserMachineLearner

class Ridge(SkLearnUserMachineLearner):

    def get_regressor(self):
        return linear_model.RidgeCV()
