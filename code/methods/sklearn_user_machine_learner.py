from sklearn import linear_model
import numpy as np
from machine_learner import MachineLearner
from code.util import user_coll
from abc import ABCMeta, abstractmethod
import math

class SkLearnUserMachineLearner(MachineLearner):
    __metaclass__ = ABCMeta

    def __init__(self, inc_avg_friend_loc=False, inc_mode_friend_loc=False, 
        inc_mode_friend_loc_weighted=False, inc_friend_count=False,
        inc_median_friend_loc=False):
        self.inc_avg_friend_loc = inc_avg_friend_loc
        self.inc_mode_friend_loc = inc_mode_friend_loc
        self.inc_mode_friend_loc_weighted = inc_mode_friend_loc_weighted
        self.inc_friend_count = inc_friend_count
        self.inc_median_friend_loc = inc_median_friend_loc
        self.clf = self.get_regressor()

    @abstractmethod
    def get_regressor(self):
        pass

    def train(self, user_train_set):
        data = []
        target = []
        for user in user_train_set:
            if user.get_x_coord() != None:
                data.append(user.get_features(self.inc_avg_friend_loc,
                    self.inc_mode_friend_loc, self.inc_mode_friend_loc_weighted,
                    self.inc_friend_count, self.inc_median_friend_loc))
                target.append(user.get_location())

        self.clf.fit(np.array(data), np.array(target))

    def predict(self, user):
        [lat, lon] = self.clf.predict(user.get_features(self.inc_avg_friend_loc,
                self.inc_mode_friend_loc, self.inc_mode_friend_loc_weighted,
                self.inc_friend_count, self.inc_median_friend_loc))

        user.set_lat(lat)
        user.set_lon(lon)