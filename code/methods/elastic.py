from sklearn import linear_model
from sklearn_user_machine_learner import SkLearnUserMachineLearner

class Elastic(SkLearnUserMachineLearner):

    def get_regressor(self):
        return linear_model.MultiTaskElasticNetCV()