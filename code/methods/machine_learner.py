from abc import ABCMeta, abstractmethod

class MachineLearner:
    __metaclass__ = ABCMeta

    @abstractmethod
    def train(self, train_set):
        pass

    @abstractmethod
    def predict(self, test_case):
        return test_set